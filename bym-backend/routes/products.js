const { Router } = require('express')
const En_product = require('../models/En_product')
const Ru_product = require('../models/Ru_product')
const auth = require('../middleware/verify')

const router = Router()

router.get('/', async (req, res) => {
    if (req.header('Accept-Language') === 'en') {
        const products = await En_product.find({}, { _id: 0 })
        res.send(products)
    } else {
        const products = await Ru_product.find({}, { _id: 0 })
        res.send(products)
    }
})

router.post('/', auth, async (req, res) => {
    if (req.user.isAdmin === false) {
        res.statusCode(403).send('Access denied')
    } else {
        if (req.body.language === 'en') {
            const prod = new En_product({
                title: req.body.title,
                color: req.body.color,
            })
            try {
                const savedProd = await prod.save()
                res.send(savedProd)
            } catch (error) {
                res.statusCode(400).send(
                    'Something went wrong while adding new product'
                )
            }
        } else {
            const prod = new Ru_product({
                title: req.body.title,
                color: req.body.color,
            })
            try {
                const savedProd = await prod.save()
                res.send(savedProd)
            } catch (error) {
                res.statusCode(400).send(
                    'Something went wrong while adding new product'
                )
            }
        }
    }
})

router.post('/many', auth, async (req, res) => {
    Ru_product.collection.insert(req.body.arr, function (err, docs) {
        if (err) {
            return console.error(err)
        } else {
            console.log('Multiple documents inserted to Collection')
        }
    })
})

router.delete('/all', auth, async (req, res) => {
    // if (req.user.isAdmin === false) {
    //     return res.status(403).send('You are not an admin')
    // }

    if (req.header('Accept-Language') === 'en') {
        try {
            await En_product.remove({})
            res.send('cleared successfully')
        } catch (error) {
            res.send('Eroor occured')
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            await Ru_product.remove({})
            res.send('cleared successfully')
        } catch (error) {
            res.send('Eroor occured')
        }
    }
})

module.exports = router
