const fs = require('fs')
const glob = require('glob')
const { Router } = require('express')
const En_recipe = require('../models/En_recipe')
const Ru_recipe = require('../models/Ru_recipe')
const User = require('../models/User')
const auth = require('../middleware/verify')
//to parse html
const cheerio = require('cheerio')

function converter(encodedData, string) {
    //convert from base64 to png
    let buff = Buffer.from(encodedData.split(';base64,').pop(), 'base64')
    let name = string + '.png'
    fs.writeFile('./static/' + name, buff, { encoding: 'base64' }, function (
        err
    ) {
        if (err) return console.log(err)
    })

    return '../' + name
}

const router = Router()

// Get all recipes for cards at home page
router.get('/all', async (req, res) => {
    const pageNo = parseInt(req.query.pageNo)
    if (pageNo < 1) {
        return res.send('Invalid page number')
    }
    const size = parseInt(req.query.size)
    if (req.header('Accept-Language') === 'en') {
        try {
            const recipes = await En_recipe.find(
                {},
                {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                    author: 1,
                    language: 1,
                }
            )
                .sort({ rating: 'desc' })
                .skip(size * (pageNo - 1))
                .limit(size)
            const pages = await En_recipe.find().countDocuments()
            if (recipes.length > 0) {
                res.send({ data: recipes, pages: Math.ceil(pages / size) })
            } else {
                res.send('No recipes here')
            }
        } catch (error) {
            // res.send('Error occured. No recipes here')
            res.send(error)
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            const recipes = await Ru_recipe.find(
                {},
                {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                    author: 1,
                    language: 1,
                }
            )
                .sort({ rating: 'desc' })
                .skip(size * (pageNo - 1))
                .limit(size)
            const pages = await En_recipe.find().countDocuments()
            if (recipes.length > 0) {
                res.send({ data: recipes, pages: Math.ceil(pages / size) })
            } else {
                res.send('Рецептов пока нет :(')
            }
        } catch (error) {
            res.send('Произошла ошибка. Рецептов пока нет :(')
        }
    }
})
// Get all favourites
router.get('/all/favourites', auth, async (req, res) => {
    const user = await User.findById(req.user._id, { favourites: 1 })
    const userFav = user.favourites
    if (userFav.length === 0)
        return res.send({ message: 'No favourites yet', error: true })

    const pageNo = parseInt(req.query.pageNo)
    if (pageNo < 1) {
        return res.send({ message: 'Invalid page number', error: true })
    }
    const size = parseInt(req.query.size)

    let pages = 1
    if (userFav.length >= size * pageNo) {
        pages = size * pageNo
    } else {
        pages = userFav.length
    }

    const recipes = []
    let recipe = null
    for (let i = size * (pageNo - 1); i < pages; i++) {
        if (userFav[i].language === 'en') {
            recipe = await En_recipe.findById(userFav[i]._id, {
                title: 1,
                preview_src: 1,
                time: 1,
                rating: 1,
                author: 1,
                language: 1,
            })
        } else if (userFav[i].language === 'ru') {
            recipe = await Ru_recipe.findById(userFav[i]._id, {
                title: 1,
                preview_src: 1,
                time: 1,
                rating: 1,
                author: 1,
                language: 1,
            })
        }
        if (recipe === null) {
            pages--
            user.favourites = user.favourites.filter(function (elem) {
                if (
                    elem.language === userFav[i].language &&
                    elem._id === userFav[i]._id
                ) {
                    return false
                } else {
                    return true
                }
            })
        } else {
            recipes.push(recipe)
        }
    }
    const newUser = await user.save()
    res.send({ error: false, data: recipes, pages: pages })
})

// Get all info about one recipe
router.get('/id/:id', async (req, res) => {
    if (req.header('Accept-Language') === 'en') {
        try {
            const recipe = await En_recipe.findById(req.params.id)
            res.send(recipe)
        } catch (error) {
            res.send('Recipe not found')
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            const recipe = await Ru_recipe.findById(req.params.id)
            res.send(recipe)
        } catch (error) {
            res.send('Такого рецепта нет')
        }
    }
})
// Search by title
router.get('/title', async (req, res) => {
    const pageNo = parseInt(req.query.pageNo)
    if (pageNo < 1) {
        return res.send('Invalid page number')
    }
    const size = parseInt(req.query.size)
    if (req.header('Accept-Language') === 'en') {
        try {
            const recipes = await En_recipe.find(
                { title: req.query.title },
                {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                    language: 1,
                    author: 1,
                }
            )
                .sort({ rating: 'desc' })
                .skip(size * (pageNo - 1))
                .limit(size)
            const pages = await En_recipe.find({
                title: req.query.title,
            }).countDocuments()
            if (recipes.length > 0) {
                res.send({ data: recipes, pages: Math.ceil(pages / size) })
            } else {
                res.send('No recipes with that title')
            }
        } catch (error) {
            res.send('Error occured. No recipes with that title')
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            const recipes = await Ru_recipe.find(
                { title: req.query.title },
                {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                    language: 1,
                    author: 1,
                }
            )
                .sort({ rating: 'desc' })
                .skip(size * (pageNo - 1))
                .limit(size)
            const pages = await En_recipe.find({
                title: req.query.title,
            }).countDocuments()
            if (recipes.length > 0) {
                res.send({ data: recipes, pages: Math.ceil(pages / size) })
            } else {
                res.send('Рецептов с таким названием нет')
            }
        } catch (error) {
            res.send('Произошла ошибка. Рецептов с таким названием нет')
        }
    }
})
// Search by inridients
router.get('/search', async (req, res) => {
    const pageNo = parseInt(req.query.pageNo)
    if (pageNo < 1) {
        return res.send('Invalid page number')
    }
    const size = parseInt(req.query.size)
    if (req.header('Accept-Language') === 'en') {
        try {
            const ingr = req.query.prod
            //https://stackoverflow.com/questions/12223465/mongodb-query-subset-of-an-array#12223544
            const recipes = await En_recipe.find(
                { ingredients: { $not: { $elemMatch: { $nin: ingr } } } },
                {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                    language: 1,
                    author: 1,
                }
            )
                .sort({ rating: 'desc' })
                .skip(size * (pageNo - 1))
                .limit(size)
            // Maybe i need to change find()
            const pages = await En_recipe.find({
                ingredients: { $not: { $elemMatch: { $nin: ingr } } },
            }).countDocuments()
            if (recipes.length > 0) {
                res.send({ data: recipes, pages: Math.ceil(pages / size) })
            } else {
                res.send('No recipes with that ingredients')
            }
        } catch (error) {
            res.send('Error occured. No recipes with that ingredients')
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            const ingr = req.query.prod
            //https://stackoverflow.com/questions/12223465/mongodb-query-subset-of-an-array#12223544
            const recipes = await Ru_recipe.find(
                { ingredients: { $not: { $elemMatch: { $nin: ingr } } } },
                {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                    language: 1,
                    author: 1,
                }
            )
                .sort({ rating: 'desc' })
                .skip(size * (pageNo - 1))
                .limit(size)
            const pages = await En_recipe.find({
                ingredients: { $not: { $elemMatch: { $nin: ingr } } },
            }).countDocuments()
            if (recipes.length > 0) {
                res.send({ data: recipes, pages: Math.ceil(pages / size) })
            } else {
                res.send('Нет рецептов с такими продуктами')
            }
        } catch (error) {
            res.send('Произошла ошибка. Нет рецептов с такими продуктами')
        }
    }
})

router.get('/favourites', auth, async (req, res) => {
    const pageNo = parseInt(req.query.pageNo)
    if (pageNo < 1) {
        return res.send('Invalid page number')
    }
    const size = parseInt(req.query.size)

    const userFav = await User.findById(req.user._id, { favourites: 1 })
    console.log(userFav)
    const recipes = []
    // for (fav of userFav.favourites) {
    //     if (fav.language === 'en') {
    //         recipes.unshift(await En_recipe.findById(fav._id))
    //     } else if (fav.language === 'ru') {
    //         recipes.unshift(await Ru_recipe.findById(fav._id))
    //     }
    // }
    userFav.favourites.reverse()
    for (
        let i = size * (pageNo - 1);
        i <
        (size * pageNo < userFav.favourites.length
            ? size * pageNo
            : userFav.favourites.length);
        i++
    ) {
        if (userFav.favourites[i].language === 'en') {
            recipes.push(
                await En_recipe.findById(userFav.favourites[i]._id, {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                })
            )
        } else if (userFav.favourites[i].language === 'ru') {
            recipes.push(
                await Ru_recipe.findById(userFav.favourites[i]._id, {
                    title: 1,
                    preview_src: 1,
                    time: 1,
                    rating: 1,
                })
            )
        }
    }
    if (recipes.length > 0) {
        res.send({
            data: recipes,
            pages: Math.ceil(userFav.favourites.length / size),
        })
    } else {
        res.send('No favourite recipes')
    }
})

// to-do: save images in fylesystem
router.post('/new', auth, async (req, res) => {
    // const preview_src = recipe.preview_src
    if (req.user.isAdmin === false) {
        return res.statusCode(403).send('You are not an admin')
    }

    if (req.header('Accept-Language') === 'en') {
        let name = Date.now() + '_'

        req.body.recipe.preview_src = converter(
            req.body.recipe.preview_src,
            name + '0'
        )

        const $ = cheerio.load(req.body.recipe.description_src)

        $('img').each(function (i, elem) {
            let source = $(this).attr('src')
            let num = i + 1
            $(this).attr('src', converter(source, name + num))
        })

        req.body.recipe.description_src = $.html()

        const recipe = new En_recipe(
            Object.assign({}, req.body.recipe, {
                author_id: req.user._id,
                author: req.user.name,
            })
        )
        try {
            const savedRecipe = await recipe.save()
            res.send(savedRecipe._id)
        } catch (error) {
            res.sendStatus(400).send(error)
        }
    } else if (req.header('Accept-Language') === 'ru') {
        let name = Date.now() + '_'

        req.body.recipe.preview_src = converter(
            req.body.recipe.preview_src,
            name + '0'
        )

        const $ = cheerio.load(req.body.recipe.description_src)

        $('img').each(function (i, elem) {
            let source = $(this).attr('src')
            let num = i + 1
            $(this).attr('src', converter(source, name + num))
        })

        req.body.recipe.description_src = $.html()

        const recipe = new Ru_recipe(
            Object.assign({}, req.body.recipe, {
                author_id: req.user._id,
                author: req.user.name,
            })
        )
        try {
            const savedRecipe = await recipe.save()
            res.send(savedRecipe._id)
        } catch (error) {
            res.sendStatus(400).send(error)
        }
    }
})

router.delete('/id/:id', auth, async (req, res) => {
    if (req.user.isAdmin === false) {
        return res.statusCode(403).send('You are not an admin')
    }

    if (req.header('Accept-Language') === 'en') {
        try {
            const recipe = await En_recipe.findById(req.params.id, {
                preview_src: 1,
            })
            const str = recipe.preview_src.slice(
                3,
                recipe.preview_src.indexOf('_')
            )

            glob('./static/' + str + '*', function (er, files) {
                for (const file of files) {
                    fs.unlink(file, (err) => {
                        if (err) throw err
                    })
                }
            })

            await En_recipe.findByIdAndDelete(req.params.id)
            res.send('deleted successfully')
        } catch (error) {
            console.log(error)
            res.send('Eroor occured')
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            const recipe = await Ru_recipe.findById(req.params.id, {
                preview_src: 1,
            })
            const str = recipe.preview_src.slice(
                3,
                recipe.preview_src.indexOf('_')
            )

            glob('./static/' + str + '*', function (er, files) {
                for (const file of files) {
                    fs.unlink(file, (err) => {
                        if (err) throw err
                    })
                }
            })

            await Ru_recipe.findByIdAndDelete(req.params.id)
            res.send('deleted successfully')
        } catch (error) {
            res.send('Eroor occured')
        }
    }
})

router.delete('/all', auth, async (req, res) => {
    if (req.user.isAdmin === false) {
        return res.status(403).send('You are not an admin')
    }

    if (req.header('Accept-Language') === 'en') {
        try {
            await En_recipe.remove({})
            res.send('cleared successfully')
        } catch (error) {
            res.send('Eroor occured')
        }
    } else if (req.header('Accept-Language') === 'ru') {
        try {
            await Ru_recipe.remove({})
            res.send('cleared successfully')
        } catch (error) {
            res.send('Eroor occured')
        }
    }
})

router.put('/rated', auth, async (req, res) => {
    if (req.body.language === 'en') {
        const user = await User.findById(req.user._id, { rated: 1 })
        const userRate = user.rated.find(function (elem) {
            if (
                elem._id === req.body._id &&
                elem.language === req.body.language
            ) {
                return true
            } else {
                return false
            }
        })
        const recipe = await En_recipe.findById(req.body._id, {
            rating: 1,
            rating_summ: 1,
            rating_number: 1,
        })

        if (userRate) {
            recipe.rating_summ =
                recipe.rating_summ + req.body.rating - userRate.rating
            recipe.rating =
                Math.round((recipe.rating_summ / recipe.rating_number) * 100) /
                100
            const updatedRecipe = await recipe.save()
            res.send('recipe updated')
        } else {
            recipe.rating_summ = recipe.rating_summ + req.body.rating
            recipe.rating_number++
            recipe.rating =
                Math.round((recipe.rating_summ / recipe.rating_number) * 100) /
                100
            const updatedRecipe = await recipe.save()
            res.send('recipe updated')
        }
    } else if (req.body.lang === 'ru') {
        const user = await User.findById(req.user._id, { rated: 1 })
        const userRate = user.rated.find(function (elem) {
            if (
                elem._id === req.body._id &&
                elem.language === req.body.language
            ) {
                return true
            } else {
                return false
            }
        })
        const recipe = await Ru_recipe.findById(req.body._id, {
            rating: 1,
            rating_summ: 1,
            rating_number: 1,
        })

        if (userRate) {
            recipe.rating_summ =
                recipe.rating_summ + req.body.rating - userRate.rating
            recipe.rating =
                Math.round((recipe.rating_summ / recipe.rating_number) * 100) /
                100
            const updatedRecipe = await recipe.save()
            res.send('recipe updated')
        } else {
            recipe.rating_summ = recipe.rating_summ + req.body.rating
            recipe.rating_number++
            recipe.rating =
                Math.round((recipe.rating_summ / recipe.rating_number) * 100) /
                100
            const updatedRecipe = await recipe.save()
            res.send('recipe updated')
        }
    }
})

router.put('/comment', auth, async (req, res) => {
    if (req.body.language === 'en') {
        try {
            const recipe = await En_recipe.findById(req.body._id)
            recipe.comments.push({
                author: req.user.name,
                text: req.body.text,
            })
            const updatedRecipe = await recipe.save()
            res.send({ error: false, data: updatedRecipe.comments })
        } catch (error) {
            res.send({
                error: true,
                data: error,
            })
        }
    } else if (req.body.language === 'ru') {
        try {
            const recipe = await Ru_recipe.findById(req.body._id)
            recipe.comments.push({
                author: req.user.name,
                text: req.body.text,
            })
            const updatedRecipe = await recipe.save()
            res.send({ error: false, data: updatedRecipe.comments })
        } catch (error) {
            res.send({
                error: true,
                data: error,
            })
        }
    }
})
module.exports = router
