const { Router } = require('express')
const User = require('../models/User')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/verify')

const router = Router()

router.get('/all', async (req, res) => {
    try {
        const users = await User.find()
        res.send(users)
    } catch (error) {
        res.send('No users yet')
    }
})

router.post('/register', async (req, res) => {
    // user exists?
    const emailExist = await User.findOne({ email: req.body.email })
    if (emailExist) return res.status(400).send('User alredy exist')

    // password hash
    const salt = await bcrypt.genSalt(10)
    const hashesPassword = await bcrypt.hash(req.body.password, salt)

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashesPassword,
    })

    try {
        const savedUser = await user.save()
        const token = jwt.sign(
            {
                _id: savedUser._id,
                isAdmin: savedUser.isAdmin,
                name: savedUser.name,
            },
            process.env.TOKEN_SECRET
        )
        res.header('Authorization', token).send(token)
    } catch (error) {
        res.sendStatus(400).send(error)
    }
})

router.post('/login', async (req, res) => {
    // check if the user exists
    const user = await User.findOne({ email: req.body.email })
    if (!user) return res.status(400).send('Wrong email')

    // check password
    const passwordValid = await bcrypt.compare(req.body.password, user.password)
    if (!passwordValid) return res.status(400).send('Invalid password')

    // create jwt
    const token = jwt.sign(
        { _id: user._id, isAdmin: user.isAdmin, name: user.name },
        process.env.TOKEN_SECRET
    )
    res.header('Authorization', token).send(token)
})

// adding or deleting favourite favourite
router.put('/favourites', auth, async (req, res) => {
    const fav_old = await User.findById(req.user._id)

    // array.includes не хочет работать как и просто some с объектом
    if (
        !fav_old.favourites
            .filter((elem) => elem._id === req.body._id)
            .some((elem) => elem.language === req.body.language)
    ) {
        fav_old.favourites.push({
            _id: req.body._id,
            language: req.body.language,
        })
        const fav_new = await fav_old.save()
        res.send(fav_new)
    } else {
        fav_old.favourites = fav_old.favourites.filter(function (elem) {
            if (
                elem._id === req.body._id &&
                elem.language === req.body.language
            )
                return false
            else return true
        })

        const fav_new = await fav_old.save()
        res.send(fav_new)
    }
})

router.get('/favourites', auth, async (req, res) => {
    try {
        const favs = await User.findById(req.user._id, { favourites: 1 })
        res.send(favs)
    } catch (error) {
        res.send('Not logged in')
    }
})

router.post('/rated', auth, async (req, res) => {
    try {
        const rated = await User.findById(req.user._id, { rated: 1, _id: 0 })

        const response = rated.rated.filter(function (elem) {
            if (
                elem._id === req.body._id &&
                elem.language === req.body.language
            )
                return true
            else return false
        })
        res.send(response)
    } catch (error) {
        res.send('Not logged in')
    }
})

router.put('/rated', auth, async (req, res) => {
    try {
        const rated_old = await User.findById(req.user._id)
        rated_old.rated = rated_old.rated.filter(function (elem) {
            if (
                elem._id === req.body._id &&
                elem.language === req.body.language
            )
                return false
            else return true
        })
        rated_old.rated.push({
            _id: req.body._id,
            language: req.body.language,
            rating: req.body.rating,
        })
        const updated = await rated_old.save()
        res.send(updated)
    } catch (error) {
        res.send('Not logged in')
    }
})

module.exports = router
