const express = require('express')
// const path = require('path')
// const fs = require('fs')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')
const history = require('connect-history-api-fallback')
const vhost = require('vhost')
//routes
const recipesRoutes = require('./routes/recipes')
const usersRoutes = require('./routes/users')
const productsRoutes = require('./routes/products')

// initializwe app
const app = express()

dotenv.config()

const PORT = process.env.PORT || 5000

// body parser middleware
app.use(express.urlencoded({ limit: '16mb', extended: true }))
app.use(express.json({ limit: '16mb', extended: true }))

app.use(cors())

//middleware for serving spa
app.use(
    history({
        htmlAcceptHeaders: ['text/html', 'application/json'],
    })
)

// static folder
app.use(express.static('static'))
app.use(vhost('en.*', express.static('../bym-front/dist')))
app.use(vhost('ru.*', express.static('../bym-front-rus/dist')))

// routes middleware
app.use('/api/recipes', recipesRoutes)
app.use('/api/users', usersRoutes)
app.use('/api/products', productsRoutes)

// Connect db first then run server
async function start() {
    try {
        await mongoose.connect(process.env.DB_CONNECT, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
        })
        app.listen(PORT, () =>
            console.log(`Server started at http://localhost:${PORT}`)
        )
    } catch (error) {
        console.log(error)
    }
}

start()
