## Ингредиенты и их количество

в бд:
Ингредиенты - массив в алфавитном порядке (потому что сортируется на клиенте перед отправкой)
Колличество - массив в том же порядке

в админке:
всё это обьект, из которого формируется массив ключей и значений
Создавать на клиенте и массив и объект. Массив отсортировать, а количество взять из объекта
Не забыть что надо использовать только те продукты, которые есть в списке

## Язык

Язык передается в хедере(Accept-Language)

## Query параметры

Query параметры надо писать так http://localhost:5000/api/recipes/search?prod[]=шоколад&prod[]=молоко
Если параметр строка - то все равно можно поставить []

## Pagination

Для пангинации используй такую запись на клиенте http://localhost:5000/api/recipes/all?size=10&pageNo=1

## Favourites

Favourites: хранятся в массиве объектов, где поля - язык, id, дата добавления. На сервере ответ формируется с помощью цикла (Bad practice)

# TODO

-   [ ] Поправить pages в recipes routes
-   [ ] Bearer type auth
-   [ ] Status codes
-   [ ] Admin functionality
-   [ ] Change responses on PUT and POST requests
-   [ ] Typo in recipes' models: not ingridients but ingredients
-   [ ] Read about CORS
-   [ ] Image compression on upload
-   [ ] Collection.remove() is deprecated
-   [ ] On recipe delete also delete it in users' ~~favourites~~ and in rated
-   [ ] Favourites added date
-   [ ] Add new ingredients in /add route
-   [ ] Email check
-   [ ] About Page
-   [ ] Russian subdomain
-   [ ] NotFound redirect
-   [ ] Account delete
-   [ ] Contenteditable plain text
-   [?] Edit recipes
-   [?] Edit comments style
