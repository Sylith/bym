const { Schema, model } = require('mongoose')

const shema = new Schema({
    title: {
        type: String,
        required: true,
    },
    color: {
        type: String,
        required: true,
    },
})

module.exports = model('En_products', shema)
