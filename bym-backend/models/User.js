const { Schema, model } = require('mongoose')

const schema = new Schema(
    {
        name: {
            type: String,
            required: true,
            min: 2,
            max: 50,
        },
        email: {
            type: String,
            required: true,
            min: 4,
        },
        password: {
            type: String,
            required: true,
            min: 6,
            max: 1024,
        },
        favourites: {
            type: Array,
            required: false,
        },
        // language: {
        //     type: String,
        //     required: false,
        // },
        isAdmin: {
            type: Boolean,
            default: false,
        },
        rated: {
            type: Array,
            required: false,
        },
    },
    { timestamps: true }
)

module.exports = model('User', schema)
