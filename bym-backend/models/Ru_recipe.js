const { Schema, model } = require('mongoose')

// instead 'schema' you can use anything
const schema = new Schema({
    title: {
        type: String,
        required: true,
    },
    preview_src: {
        type: String,
        required: true,
    },
    ingredients: {
        type: Array,
        required: true,
    },
    quantity: {
        type: Array,
        required: true,
    },
    time: {
        type: String,
        required: true,
    },
    rating: {
        type: Number,
        required: true,
        default: 0,
    },
    rating_summ: {
        type: Number,
        default: 0,
    },
    rating_number: {
        type: Number,
        default: 0,
    },
    description_src: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        default: 'BYM team',
    },
    comments: {
        type: Array,
        required: false,
    },
    author_id: {
        type: String,
        required: true,
    },
    language: {
        type: String,
        default: 'ru',
    },
})

//('Any Name', schema)
module.exports = model('Ru_recipe', schema)
