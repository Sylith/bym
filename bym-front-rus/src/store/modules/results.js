import { API_URL } from '../../urls'

export default {
    actions: {
        async searchByTtl(ctx) {
            const res = await fetch(
                API_URL +
                    '/recipes/title' +
                    ctx.state.queryString +
                    ctx.state.title,
                {
                    headers: {
                        'Accept-Language': ctx.state.language,
                    },
                }
            )
            const recipes = await res.json()
            ctx.commit('updateRecipes', {
                recipes: recipes.data,
                pages: recipes.pages,
            })
        },
        async searchByIngr(ctx) {
            const res = await fetch(
                API_URL +
                    '/recipes/search' +
                    ctx.state.queryString +
                    ctx.state.ingredients,
                {
                    headers: {
                        'Accept-Language': ctx.state.language,
                    },
                }
            )
            const recipes = await res.json()
            ctx.commit('updateRecipes', {
                recipes: recipes.data,
                pages: recipes.pages,
            })
        },
    },
    mutations: {
        updateRecipes(state, payload) {
            state.recipes = payload.recipes
            state.pages = payload.pages
        },
        updateIngredients(state, payload) {
            state.ingredients = payload.ingredients
            state.isSearchByIngr = true
            state.language = payload.language
        },
        updateTtl(state, payload) {
            state.title = payload.title
            state.isSearchByIngr = false
            state.language = payload.language
        },
        updateQuery(state, payload) {
            state.queryString = payload.queryString
        },
    },
    state: {
        recipes: [],
        queryString: '?size=12&pageNo=1',
        title: '',
        ingredients: '',
        isSearchByIngr: true,
        pages: 1,
        language: 'ru',
    },
    getters: {
        resultsGetter(state) {
            return state.recipes
        },
    },
}
