import { API_URL } from '../../urls'

export default {
    actions: {
        // get all recipes, home page
        async fetchAll(ctx, payload) {
            const res = await fetch(
                API_URL + '/recipes/all' + payload.queryString,
                {
                    headers: {
                        'Accept-Language': 'ru',
                    },
                }
            )
            const recipes = await res.json()
            ctx.commit('updateAll', {
                recipes: recipes.data,
                pages: recipes.pages,
            })
        },
        // search by ingridients
        // async fetchInhg(ctx) {},

        async fetchFavs(ctx) {
            if (localStorage.getItem('Authorization')) {
                const res = await fetch(API_URL + '/users/favourites', {
                    method: 'GET',
                    headers: {
                        Authorization: localStorage.getItem('Authorization'),
                    },
                })
                const favs = await res.json()
                ctx.commit('updateFavs', {
                    favs: favs.favourites,
                })
            }
        },

        async fetchAllFavs(ctx, payload) {
            const res = await fetch(
                API_URL + '/recipes/all/favourites' + payload.queryString,
                {
                    headers: {
                        'Accept-Language': 'ru',
                        Authorization: localStorage.getItem('Authorization'),
                    },
                }
            )
            const recipes = await res.json()
            ctx.commit('updateFavsRecipes', {
                recipes: recipes.data,
                pages: recipes.pages,
            })
        },
    },
    mutations: {
        updateAll(state, payload) {
            state.recipes = payload.recipes
            state.pages = payload.pages
        },
        updateFavsRecipes(state, payload) {
            state.favsRecipes = payload.recipes
            state.favsRecipesPages = payload.pages
        },
        updateFavs(state, payload) {
            state.favs = payload.favs
        },
    },
    state: {
        recipes: [],
        pages: 1,
        favs: [],
        favsRecipes: [],
        favsRecipesPages: 1,
    },
    getters: {
        allGetter(state) {
            return state.recipes
        },
        allFavsRecipesGetter(state) {
            return state.favsRecipes
        },
        // isFavGetter: (state) => (payload) => {
        //     if (
        //         state.favs.some(
        //             (el) =>
        //                 el._id === payload._id &&
        //                 el.language === payload.language
        //         )
        //     ) {
        //         return true
        //     } else {
        //         return false
        //     }
        // },
    },
}
