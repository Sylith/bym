import Vue from 'vue'
import VueRouter from 'vue-router'
import Start from '../views/Start.vue'
import Home from '../views/start_kids/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        // name: "Start",
        component: Start,
        children: [
            {
                path: '/',
                name: 'Home',
                component: Home,
            },
            {
                path: '/about',
                name: 'About',
                component: () => import('../views/start_kids/About.vue'),
            },
            {
                path: '/favourites',
                name: 'Favourites',
                component: () => import('../views/start_kids/Favourites.vue'),
                beforeEnter: (to, from, next) => {
                    if (localStorage.getItem('Authorization')) next()
                    else next('/auth')
                },
            },
            {
                path: '/recipe',
                name: 'Recipe',
                component: () => import('../views/start_kids/Recipe.vue'),
                props: (route) => ({
                    language: route.query.lang,
                    _id: route.query.id,
                }),
            },
            {
                path: '/results',
                name: 'Results',
                component: () => import('../views/start_kids/Results.vue'),
            },
            {
                path: '/add',
                name: 'Add',
                component: () => import('../views/start_kids/Add.vue'),
                beforeEnter: (to, from, next) => {
                    if (localStorage.getItem('Authorization')) {
                        let token = localStorage.getItem('Authorization')
                        const parseJwt = (token) => {
                            try {
                                return JSON.parse(atob(token.split('.')[1]))
                            } catch (e) {
                                return null
                            }
                        }
                        if (parseJwt(token).isAdmin === true) {
                            next()
                        } else {
                            next('/403')
                        }
                    } else {
                        next('/auth')
                    }
                },
            },
            {
                path: '/auth',
                name: 'Auth',
                component: () => import('../views/start_kids/Auth.vue'),
            },
        ],
    },
    {
        path: '/search',
        name: 'Search',
        component: () => import('../views/Search.vue'),
    },
    {
        path: '/403',
        name: 'notAllowed',
        component: () => import('../views/403.vue'),
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
})

export default router
