import Vue from "vue";
import Vuex from "vuex";
import allRecipes from "../store/modules/allRecipes";
import results from "../store/modules/results";
import ingredients from "./modules/ingredients";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    allRecipes,
    results,
    ingredients
  }
});
