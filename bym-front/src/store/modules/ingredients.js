import { API_URL } from "../../urls";

export default {
  actions: {
    async fetchIngr(ctx) {
      const res = await fetch(API_URL + "/products", {
        headers: {
          "Accept-Language": "en"
        }
      });
      const ingr = await res.json();
      ctx.commit("updateIngr", ingr);
    }
  },
  mutations: {
    updateIngr(state, ingr) {
      state.ingredients = ingr;
    }
  },
  state: {
    ingredients: []
  },
  getters: {
    allIngredients(state) {
      return state.ingredients;
    }
  }
};
