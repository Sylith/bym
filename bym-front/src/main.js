import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import wysiwyg from 'vue-wysiwyg-fixed'
Vue.use(wysiwyg, {
    // { [module]: boolean (set true to hide) }
    hideModules: {
        separator: true,
        justifyLeft: true,
        justifyCenter: true,
        justifyRight: true,
        code: true,
    },

    // you can override icons too, if desired
    // just keep in mind that you may need custom styles in your application to get everything to align
    // iconOverrides: { bold: "<i class='your-custom-icon'></i>" },

    // if the image option is not set, images are inserted as base64
    // image: {
    //   uploadURL: "/api/myEndpoint",
    //   dropzoneOptions: {}
    // },

    // limit content height if you wish. If not set, editor size will grow with content.
    // maxHeight: '500px',

    // set to 'true' this will insert plain text without styling when you paste something into the editor.
    forcePlainTextOnPaste: true,

    // specify editor locale, if you don't specify this, the editor will default to english.
    locale: 'en',
})
Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app')
